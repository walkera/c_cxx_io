#include "cimpl.h"
#include "cppimpl.h"
#include <cstdint>
#include <iostream>

void test1()
{
    io_wrapper wrapper;
    ci_init_iowrapper(&wrapper);

    std::uint8_t data[] = { 1, 2, 3, 4, 5};
    std::uint8_t res[10];
    
    fhandle_t h;
    h = ci_open("tmp.txt", "w");
    wrapper.writeptr(h, (void*)data, 5);
    wrapper.closeptr(h);

    h = ci_open("tmp.txt", "r");
    int n = wrapper.readptr(h, (void*)res, 5);
    wrapper.closeptr(h);

    for(int i = 0; i < n; i++) {
        std::cout << (int)res[i] << std::endl;
    }
}

void test2()
{
    io_wrapper wrapper;
    cppi_init_iowrapper(&wrapper);

    std::uint8_t data[] = { 1, 2, 3, 4, 5};
    std::uint8_t res[10];
    
    fhandle_t h;
    h = cppi_open("tmp.txt", "w");
    wrapper.writeptr(h, (void*)data, 5);
    wrapper.closeptr(h);

    h = cppi_open("tmp.txt", "r");
    int n = wrapper.readptr(h, (void*)res, 5);
    wrapper.closeptr(h);

    for(int i = 0; i < n; i++) {
        std::cout << (int)res[i] << std::endl;
    }
}

int main()
{
    test1();
    test2();
    return 0;
}
