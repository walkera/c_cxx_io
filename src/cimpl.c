#include "cimpl.h"

#include <unistd.h>
#include <stdio.h>

int read_proxy(fhandle_t h, void* data, int len) {
    return fread(data, 1, len, (FILE*)h);
}

int write_proxy(fhandle_t h, void* data, int len) {
    return fwrite(data, 1, len, (FILE*)h);
}

int close_proxy(fhandle_t h) {
    return fclose((FILE*)h);
}

fhandle_t ci_open(const char* name, const char* mode) {
    return (fhandle_t)fopen(name, mode);
}

void ci_init_iowrapper(io_wrapper* wrapper)
{
    (*wrapper).readptr  = read_proxy;
    (*wrapper).writeptr = write_proxy;
    (*wrapper).closeptr = close_proxy;
}
