/**
 * This is a C header with protection if it's included in C++
 */
#ifndef CPPIMPL_H
#define CPPIMPL_H

#include "core.h"


#ifdef __cplusplus
extern "C" {
#endif 

fhandle_t cppi_open(const char* name, const char* mode);
void cppi_init_iowrapper(io_wrapper* wrapper);

#ifdef __cplusplus
}
#endif 

#endif
