/**
 * This is a C header with protection in case it's included in a C++ file
 */
#ifndef CIMPL_H
#define CIMPL_H

#include "core.h"

#ifdef __cplusplus
extern "C" {
#endif 

fhandle_t ci_open(const char* name, const char* mode);
void ci_init_iowrapper(io_wrapper* wrapper);

#ifdef __cplusplus
}
#endif 

#endif
