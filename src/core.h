/**
 * This is a C header with protection in case it's included in a C++ file
 */
#ifndef IOCORE_H
#define IOCORE_H

#ifdef __cplusplus
extern "C" {
#endif 

typedef void* fhandle_t;
typedef int (*read_func)(fhandle_t, void*, int);
typedef int (*write_func)(fhandle_t, void*, int);
typedef int (*close_func)(fhandle_t);
typedef struct {
    read_func  readptr;
    write_func writeptr;
    close_func closeptr;
} io_wrapper;

#ifdef __cplusplus
}
#endif 

#endif // core.h
