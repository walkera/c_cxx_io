#include "cppimpl.h"
#include <fstream>

namespace /* anonymous */ {

    class objport
    {
    public:
        objport(const char* name) 
            : strm_(name)
        {
        }

        int read(void* data, int len) {
            strm_.read((char*)data, len);
            if(strm_) {
                return len;
            }
            return strm_.gcount();
        }

        int write(void* data, int len) {
            strm_.write((char*)data, len);
            return len;
        }

    private:
        std::fstream strm_;
    };

}

int read_proxy(fhandle_t h, void* data, int len) {
    return ((objport*)h)->read(data, len);
}

int write_proxy(fhandle_t h, void* data, int len) {
    return ((objport*)h)->write(data, len);
}

int close_proxy(fhandle_t h) {
    objport* p = (objport*)h;
    delete p;
    return 0;
}

#ifdef __cplusplus
extern "C" {
#endif 

fhandle_t cppi_open(const char* name, const char* mode) {
    return new objport(name);
}

void cppi_init_iowrapper(io_wrapper* wrapper) {
    wrapper->readptr  = read_proxy;
    wrapper->writeptr = write_proxy;
    wrapper->closeptr = close_proxy;
}

#ifdef __cplusplus
}
#endif 
